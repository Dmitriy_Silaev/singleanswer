﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.Net.Sockets;

namespace Answer485
{
    public partial class Form1 : Form
    {
        public static Mutex mtxcom1 = new Mutex();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            // передаем в конструктор тип класса
            XmlSerializer formatter1 = new XmlSerializer(typeof(Com));
           
            // десериализация
            using (FileStream fs1 = new FileStream("comone.xml", FileMode.OpenOrCreate))
            {
                try
                {
                    Com newPerson1 = (Com)formatter1.Deserialize(fs1);

                    //Console.WriteLine("Имя: {0} --- Возраст: {1}", newPerson.Name, newPerson.Baudrate);
                    if (!serialPort1.IsOpen)
                    {
                        serialPort1.PortName = newPerson1.Name;
                        serialPort1.BaudRate = newPerson1.Baudrate;//comboBox1.SelectedItem.ToString();
                        serialPort1.Open();
                        //Com1PortExist = 1;
                        label1.Text = serialPort1.PortName;
                        label2.Text = serialPort1.BaudRate.ToString();
                        comboBox1.Text = serialPort1.BaudRate.ToString();
                    }
                }
                catch (InvalidOperationException)
                {
                    //MessageBox.Show("Настройте конфигурацию");
                    //Close();
                    groupBox1.BackColor = Color.LightPink;
                }
                catch (UnauthorizedAccessException)
                {
                    //MessageBox.Show("Отказ в доступе к порту load");
                    groupBox1.BackColor = Color.LightPink;
                }
                catch (ArgumentNullException)
                {
                    ///пустота нет такого значения
                    groupBox1.BackColor = Color.LightPink;
                }
                catch (IOException)
                {
                    //MessageBox.Show("Отсутствует ком порт");
                    groupBox1.BackColor = Color.LightPink;
                    //Close();
                }
            }

         

            try
            {
                string[] str = System.IO.Ports.SerialPort.GetPortNames();
                //this.listBox1.Items.AddRange(new object[] {"456"});
                listBox1.Items.AddRange(str);
                listBox1.SetSelected(0, true);
                listBox1.SelectedItem = serialPort1.PortName;
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("Подключите устройство к порту (нет ком порта)");
                Close();
            }



          
                var pollingThread = new Thread(Polling1);
                pollingThread.IsBackground = true;
                pollingThread.Start();
          

            //////////////////////////////////////////////////////////////////////////        
        }
        private void Polling1()
        {
            byte[] InBuffer = new byte[1024];
            int Count = 0;
            while (true)
            {
                Count = 0;
                if (serialPort1.IsOpen)
                {
                    try
                    {
                        serialPort1.ReadTimeout = -1;
                        InBuffer[0] = (byte)serialPort1.ReadByte();
                    }
                    catch (Exception ex)
                    {
                        serialPort1.ReadTimeout = -1;
                        continue;
                    }
                    Count++;
                    mtxcom1.WaitOne();
                    int i = 1;
                    serialPort1.ReadTimeout = 200;
                    try
                    {
                        for(i=1; i<1024; i++)
                        {                             
                             InBuffer[i] = (byte)serialPort1.ReadByte();
                        }
                        /*
                        while (serialPort1.BytesToRead != 0)
                        {

                            byte[] data = new byte[serialPort1.BytesToRead];
                            serialPort1.Read(data, 0, data.Length);
                            Array.Copy(data, 0, InBuffer, 1, data.Length);
                            Count += data.Length;
                        }
                        */
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show("Не удалось получить ответ"); 
                        serialPort1.ReadTimeout = -1;
                        mtxcom1.ReleaseMutex();
                        //continue;
                    }
                   // mtxcom1.ReleaseMutex();
                    try
                    {
                        mtxcom1.WaitOne();
                        serialPort1.Write(InBuffer, 0, i);//Count
                        mtxcom1.ReleaseMutex();
                    }
                    catch (Exception ex)
                    {
                        serialPort1.ReadTimeout = -1;
                        mtxcom1.ReleaseMutex();
                        continue;
                    }

                }
                else
                {
                    Thread.Sleep(50);
                }
            }
        }


        private void button1_Click(object sender, EventArgs e)//открыть порт
        {

            try
            {
                

                mtxcom1.WaitOne();
                if (serialPort1.IsOpen) serialPort1.Close();
                ///mtxcom1.ReleaseMutex();

                //mtxcom1.ReleaseMutex();
                //mtxcom1.WaitOne();
                serialPort1.PortName = listBox1.SelectedItem.ToString();
                serialPort1.BaudRate = int.Parse(comboBox1.SelectedItem.ToString());//
                mtxcom1.ReleaseMutex();
                mtxcom1.WaitOne();

                serialPort1.Open();
                label1.Text = serialPort1.PortName;
                label2.Text = serialPort1.BaudRate.ToString();

                groupBox1.BackColor = System.Drawing.SystemColors.Control;
                // объект для сериализации
                Com com = new Com(serialPort1.PortName, serialPort1.BaudRate);
               

                // передаем в конструктор тип класса
                XmlSerializer formatter = new XmlSerializer(typeof(Com));

                // получаем поток, куда будем записывать сериализованный объект
                using (FileStream fs = new FileStream("comone.xml", FileMode.Truncate))
                {
                    formatter.Serialize(fs, com);
                }

                mtxcom1.ReleaseMutex();


            }
            catch (Exception ex)//
            {
                groupBox1.BackColor = Color.LightPink;
                mtxcom1.ReleaseMutex();
            }



         


           

        }
        [Serializable]

        public class Com
        {

            public string Name { get; set; }
            public int Baudrate { get; set; }




            // стандартный конструктор без параметров
            public Com()
            { }

            public Com(string name, int baudrate)
            {

                Name = name;
                Baudrate = baudrate;

            }
        }

        public class Ip
        {

            public string IpNum { get; set; }
            public int Port { get; set; }
            public int HostNum { get; set; }




            // стандартный конструктор без параметров
            public Ip()
            { }

            public Ip(string ipnum, int port, int hostnum)
            {

                IpNum = ipnum;
                Port = port;
                HostNum = hostnum;

            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
